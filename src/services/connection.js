import mysql from 'mysql';
import { configs } from '../configs';

export class connection {
    conn;
    configs;

    constructor() {
        this.configs = new configs();

        this.conn = this.getConnection();
        
    }
    
    getConnection(){
        if(!this.conn) {
                this.conn = mysql.createConnection(this.configs.DB);
            }

            return this.conn;
    }

    closeConnection() {
          this.conn.end();
    }
}
