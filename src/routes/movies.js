import express from 'express';
var router = express.Router();

import { movies } from '../dao/models/movies';
import { moviedao } from '../dao/moviedao';


/* POST movies/ - adiciona um filme */
router.post('/', async (req, res, next) =>  {
  if(req.body && req.body.title && req.body.director){
    const m = Object.assign(new movies(), req.body);
    const mDao = await new moviedao().insert(m);
    
    try {
      res.json({"message" : `Filme foi inserido com sucesso.`});
  
    } catch (err) {
      console.log(err);
      res.json({"message" : `Filme não foi inserido.`, "error": err});
      
    }
  }
});


/* DELETE movies/ - remove um filme */
router.delete('/', async function(req, res, next) {
  if(req.body && req.body.movies_id){
      const c = Object.assign(new movies(), req.body);
      const del = await new moviedao().deleteMovies(c)
    
      try {
        console.log(del);
        res.json({"message" : `Filme foi removido com sucesso`});
        
        
      } catch (err) {
        console.log(err);
    
        res.json({"message" : `Filme não foi removido`, "error": err});
    
      }
  }
});

/**
 * POST movies/search - Retorna um filme por qualquer parametro do objeto {movies_id?number, title?string, director?string}
 */
router.post('/search', async (req, res, next) => {
  if(req.body && (req.body.title || req.body.director)){
    const m = await new moviedao().select(Object.assign(new movies(), req.body));
    res.json({"message" : `busca realizada com sucesso`, "data": m});
    
  } else {
    res.json({"message" : `Peencha title ou director para fazer uma busca por um filme`});
    
  }
});


/**
 *  GET movies/ - Retorna todos os filmes cadastrados
 *
 */
router.get('/', async (req, res, next) => {
  const m = await new moviedao().selectAll();
  if(m && m.length > 0) {
    res.json({"message" : `Todos os filmes cadastrados`, "data": m});
    
  } else {
    res.json({"message" : `Nenhum filme cadastrado`});
    
  }
});

/**
 * GET movies/movies_id - Retorna um filme pelo ID
 */
router.get('/:movies_id', async (req, res, next) => {
  const m = await new moviedao().select(req.params);
  console.log(m);

  if(m) {
    res.json({"message" : `Um filme`, "data": m});

  } else {
    res.json({"message" : `nenhum filme encontrado com id`, "data": req.params});
    
  }
});



module.exports = router;