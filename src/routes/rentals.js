import express from 'express';
import jwt from 'jsonwebtoken';
import { configs } from '../configs';

import { rentals } from '../dao/models/rentals';
import { rentaldao } from '../dao/rentaldao';

var router = express.Router();


/**
 * Retorna todos os filmes alugados
 */
router.get('/', async function(req, res, next) {
  const r = await new rentaldao().selectAll();
  
  res.json({"message" : `busca realizada com sucesso`, "data": r});
});




/**
 * middleware para verificação do token   
 */
const verifyToken = (req, res, next) => {
  const bearerHeader = req.headers['authorization'];

  if (typeof bearerHeader !== 'undefined') {
    const bearer = bearerHeader.split(' ')[1];
    req.token = bearer;

  } else {
    res.sendStatus(403)
  }


  next();
}

/* POST rentals/ - efetua o aluguel de um filme */
/**
 * 
 ex:
  curl -X POST \
  http://localhost:3000/rentals/ \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnQiOnsiY2xpZW50c19pZCI6MTEsIm5hbWUiOiJDbGViZXIgSGVuc2VsIiwiZW1haWwiOiJjbGViZXJoZW5zZWxAZ21haWwuY29tIiwiY3JlYXRlZF9hdCI6IjIwMTgtMDUtMTQgMDY6MjE6NDcifSwiaWF0IjoxNTI2NDA2NDQ1LCJleHAiOjE1MjcwMTEyNDV9.IkZKeQpNYKXQnn0IkCGpP0VZSnXMxv9bZoBLf8wt160' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d 'clients_id=12&movies_id=5'
 
  */
router.post('/', verifyToken, async (req, res, next) => {
  jwt.verify(req.token, new configs().jwtSecret, async (err, authData) => {
    if (err) {
        res.sendStatus(403);
    } else {
      const r = Object.assign(new rentals(), req.body);
      const rDao = await new rentaldao().insertRent(r);
    
      try {
          res.json({"message" : `${rDao}`});
        
      } catch (err) {
        console.log(err);
    
        res.json({"message" : `Filme foi alugado.`, "error": err});
      }
    }
  });
  
});

/* POST rentals/devolution - devolução de um filme alugado */
router.put('/devolution', async (req, res, next) => {
  /**
   * @todo validar dados de entrada
   */
  const r = Object.assign(new rentals(), req.body);
  let rDao = new rentaldao();
  
  if(r.clients_movies_id || r.movies_id && r.clients_id){
    rDao = await rDao.devolution(r);
  }
  
  try {
    res.json({"message" : `Filme foi devolvido.`});
    
  } catch (err) {
    console.log(err);
    res.json({"message" : `Filme não foi devolvido.`, "error": err});
  }
  
});

module.exports = router;