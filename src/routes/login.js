
import express from 'express';
import jwt from 'jsonwebtoken';
import { configs } from '../configs';

const router  = express.Router();

import { clients } from '../dao/models/clients';
import { clientdao } from '../dao/clientdao';

/**
 * POST / - Cria e envia o token para o usuário autenticado
 */
router.post('/login', async function (req, res, next) {
    if (req.body && req.body.email && req.body.password) {
        console.log('login...');

        const c = Object.assign(new clients(), req.body);
        const client = await new clientdao().select(c);

        try {
            if (client) {
                delete client.password;
                
                jwt.sign({ client }, new configs().jwtSecret, { expiresIn: '7d'}, (err, token) => {
                    res.json({ token })
                })
            } else if (req.body && req.body.email === 'admin' && req.body.password === 'admin'){
                jwt.sign({ client }, new configs().jwtSecretAdmin, { expiresIn: '7d'}, (err, token) => {
                    res.json({"message": "Iniciando como ADMIN", "token": token })
                })
            } else {
                res.json({"message" : "Nenhum usuário encontrado com a identificação", "id_" : req.body});
                
            }
        

        } catch (err) {

            console.log(err);
            res.json({"message" : "Ocorreu algum problema no login", "error" : err});
        }
        

    }

});

/**
 * GET /logout
 */
router.get('/logout', () => {
    
})
module.exports = router;




