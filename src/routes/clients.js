import express from 'express';
var router = express.Router();

import { clients } from '../dao/models/clients';
import { clientdao } from '../dao/clientdao';

/**
 *  GET clients - Retorna todos os clientes cadastrados
 *
 */
router.get('/', async function(req, res, next) {
  const c = await new clientdao().selectAll()
  
  c.map((c) => { 
    delete c.password;
    return c;  
  });
  res.json({"message" : `busca realizada com sucesso`, "data": c});
});

/**
 * GET clients/clients_id - Retorna um cliente pelo ID
 */
router.get('/:clients_id', async function(req, res, next) {
  const cDao = await new clientdao().select(req.params)
  
  try {
    
    delete cDao.password;
    res.json({"message" : `Cliente foi encontrado`, "data" : cDao});
    
  } catch (err) {
    console.log(err);
    res.json({"message" : `Nenhum cliente encontrado`, "error" : err});
    
  }
});

/* POST insert - valida e salva e sava um cliente */
router.post('/', async function(req, res, next) {
  if (req.body && req.body.name && req.body.email && req.body.password) {
    const c = Object.assign(new clients(), req.body);
    const cDao = await new clientdao().insert(c);
    
    try {
      //console.log(cDao.model);
      res.json({"message" : `Cliente inserido com sucesso`});

    } catch (err) {
      console.log(err);
      res.json({"message" : `Ocorreu algum problema ao adicionar um cliente`, "error" : err});
    }

  }
});

/* DELETE delete - remove um cliente */
router.delete('/', async function(req, res, next) {
  if(req.body && req.body.clients_id) {
    const c = Object.assign(new clients(), req.body);
    const del = await new clientdao().deleteClient(c)
  
    try {
      res.json({"message" : `Cliente removido com sucesso`});
  
    } catch (err) {
      console.log(err);
  
      res.json({"message" : `Ocorreu um problema ao remover o cliente`, "error": err});
      
    }
  }
});

module.exports = router;