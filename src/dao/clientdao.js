import { dao } from './dao';
import { clients } from './models/clients';

export class clientdao extends dao {
    constructor () {
        super('clients');
    }

    /**
     * Remove as foreingkey antes de remover o cliente
     * @param {client} client cliente que será removido (depois das foreingkey)
     * @returns {Promise<dao>} Retorno do delete de um cliente
     * 
     */
    deleteClient(client) {
        return new Promise((res, rej) => {
            if(!client) rej(new Error('id não pode ser vazio'));
            
            this.model = client;
            const param = `${this.table}_id`;

            this.conn.query(`DELETE FROM clients_movies WHERE clients_id = ${param}`, (error, results, fields) => {
                if (error) {
                    console.error(err);
                    rej(error);

                } else {
                    //console.log(results);
                    this.delete(client);
                    res(this);
                }

            });
        });
    }
}