import { dao } from './dao';
import { rentals } from './models/rentals';

export class rentaldao extends dao {
    constructor () {
        super('clients_movies');
        
    }
    /**
     * Responsável pela devolução do filme
     * @param {movie} movie - Atualiza a data de devolução do filme
     */
    devolution(obj) {
        return new Promise((res, rej) => {
            if (obj) {
                obj.devolution = this._DATETIME();
                this.model = obj;
                
                const up = this.update(this.model);

                try {
                    res(this.model);

                } catch (err) {
                    console.log(err)
                    rej(err);
                }

            }

        })
    
    }

    /**
     * Responsável por alugar o filme
     * @param {movie} movie - filme que será alugado
     * @returns {Promise<dao>} Adiciona um filme como alugado
     */
    insertRent(obj) {
        return new Promise(async (res, rej) => {
            if(obj) {
                const s = await this.select(Object.assign(new rentals(), {movies_id: obj.movies_id}));
                if(s){
                    const err = new Error(`Filme já está locado`);
                    //console.error(err);
                    res(`Filme já estava locado`);    
                } else {

                    const d = await this.insert(obj)
                    res(`Filme foi alugado com sucesso.`);
                }

            } else {
                const err = new Error(`objeto para ${this.table} está vazio`);
                console.error(err);

                rej(error);
            }
        });
    }
}