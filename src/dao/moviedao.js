import { dao } from './dao';
import { movies } from './models/movies';

export class moviedao extends dao {
    constructor () {
        super('movies');
    }

    /**
     * Remove foreingkey antes de remover o filme
     * 
     * @param {movie} movie filme que será removido
     * @returns {Promise<dao>} Retorna o this
     * 
     */
    deleteMovie(obj) {
        return new Promise((res, rej) => {
            if(!obj) rej(new Error('id não pode ser vazio'));
            
            this.model = obj;
            const param = `${this.table}_id`;

            this.conn.query(`DELETE FROM clients_movies WHERE clients_id = ${param}`, (error, results, fields) => {
                if (error) {
                    console.log(err)
                    rej(error);

                } else {
                    console.log(results);
                    this.delete(obj);
                    res(this);

                }
            });
        });
    }
}