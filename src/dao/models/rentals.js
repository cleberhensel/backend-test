export class rentals {
    clients_id;
    movies_id;
    
    /**
     * @param {number} clients_id - ID do cliente
     * @param {number} movies_id - ID do filme
     */
    constructor(clients_id, movies_id) {
        this.clients_id = clients_id;
        this.movies_id = movies_id;
    }
}