export class movies {
    title;
    director;
    
    /**
     * Model de movies
     * @param {string} title - Titulo do filme
     * @param {string} director - Diretor do filme
     */
    constructor(title, director) {
        this.title = title;
        this.director = director;
    }
}