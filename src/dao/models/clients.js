export class clients {
    name;
    email;
    password;
    
    /**
     * Model de client
     * 
     * @param {string} name Nome do cliente
     * @param {string} email Email do cliente
     * @param {string} password senha do cliente
     */
    constructor(name, email, password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }
}