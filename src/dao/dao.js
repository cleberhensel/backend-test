import { connection } from '../services/connection';
import moment from 'moment';

/**
 * Classe que abstrai a lógica de insert/update/select/selecteAll/delete
 */
export class dao {
    model;
    table;
    conn;
    /**
     * 
     * @param {string} table nome da tabela do banco que irá extends das funcionalidade de DAO
     */
    constructor(table) {
        this.table = table;
        this.conn = new connection().conn;
    }

    /**
     * Abstração para SELECT * FROM [TABLE] WHERE [TABLE]_id = id
     * @param {number} id id que será buscado no DB
     * @returns {Promise<any>} retorna um objeto do db
     */
    select(obj) {
        return new Promise((res, rej) => {
            if(obj) {
                this.model = obj;

                let mount = []; 
                Object.keys(obj).map((field) => {
                    if (obj[field]) {
                        const value = (typeof obj[field] === 'string')? `"${obj[field].toString()}"`: obj[field]; 
                        mount.push(`${field} = ${value}`);
                    }
                })

                const q = `SELECT * FROM ${this.table} WHERE ${mount.join(' AND ')}`;
                
                this.conn.query(q, (error, results, fields) => {
                    if (results) {
                        res(results[0]);
                    
                    } else {
                        const err = new Error(`Nenhum informação encontrada ${q}`);
                        console.log(err);
                        res([]);
                    } 
                });
            } else {
                const err = new Error('id não pode ser vazio');
                console.error(err);
                res(error);

            }

        });
    }
    /**
     * Abstração para SELECT * FROM [TABLE]
     * @returns {Promise<any>} retorna vários objetos do db
     */
    selectAll() {
        return new Promise((res, rej) => {
            const q = `SELECT * FROM ${this.table}`;
            this.conn.query(q, (error, results, fields) => {
                if (results) {
                    console.log(results);
                    res(results);
                    
                } else {
                    const err = new Error(`Erro durante a consulta: ${q}`);
                    console.error(err);
                    rej(error);

                } 
            });
        });
    }
    /**
     * Abstração para insert
     * @returns {Promise<dao>} Retorno do insert no db
     */
    insert(obj) {
        return new Promise((res, rej) => {
            if(obj) {
                this.model = obj;
                const q = `INSERT INTO ${this.table} SET ?`
                this.conn.query(q, this.model, (error, results, fields) => {
                    if (results) {
                        console.log(`dados inseridos na tabela - ${this.table}`);
                        res(this);
                    
                    } else {
                        const err = new Error(`Erro durante o insert: ${q}`);
                        console.error(err);
                        console.log(this.model);
    
                        rej(error);
    
                    }
    
                });
            } else {
                const err = new Error(`objeto para ${this.table} está vazio`);
                console.error(err);

                rej(error);
            }
        });
    }

    /**
     * Abstração do update 
     * @param {obj} objeto que será removido
     * @returns {Promise<dao>} Retorno do delete de uma linha
     */
    update(obj) {
        return new Promise((res, rej) => {
            if(obj && obj[`${this.table}_id`] !== undefined) {
                this.model = obj;

                let mount = []; 
                Object.keys(obj).map((field) => {
                    if (obj[field]) {
                        console.log(`${field} - ${typeof obj[field]}`)
                        
                        const value = (typeof obj[field] === 'string')? `"${obj[field].toString()}" `: obj[field]; 
                        
                        if(field !== `${this.table}_id`) {
                            mount.push(`${field} = ${value}`);
                        }
                    }
                })

                const param = obj[`${this.table}_id`];//parametro [TABLE]_id
                const q = `UPDATE ${this.table} SET ${mount.join(' ,')} WHERE ${this.table}_id = ${param}`; // query para remoção 
                
                console.log(q);

                this.conn.query(q, (error, results, fields) => {
                    if (!error) {
                        console.log(`Update na tabela - ${this.table}`);
                        res(this);
                    } else {
                        const err = new Error(`Erro durante update: ${q}`);
                        console.error(err);
                        console.log(this.model);
                        rej(error);
    
                    } 
                });
                
            } else {
                const err = new Error(`objeto para ${this.table} está vazio ou ${param} é undefined`);
                console.error(err);

                rej(error);

            }
            
            
        });
    }

    /**
     * Abstração do Delete 
     * @param {obj} objeto que será removido
     * @returns {Promise<dao>} Retorno do delete de uma linha
     * 
     */
    delete(obj) {
        return new Promise((res, rej) => {
            if(obj && obj[`${this.table}_id`] !== undefined) {
                this.model = obj;
                const param = this.model[`${this.table}_id`];//parametro [TABLE]_id
                const q = `DELETE FROM ${this.table} WHERE ${this.table}_id = ${param}`; // query para remoção 
                
                this.conn.query(q, (error, results, fields) => {
                    if (!error) {
                        console.log(`removido com sucesso da tabela - ${this.table}`);
                        res(this);
                    } else {
                        const err = new Error(`Erro durante o delete: ${q}`);
                        console.error(err);
                        rej(error);
    
                    } 
                });
                
            } else {
                const err = new Error(`objeto para ${this.table} está vazio ou ${param} é undefined`);
                console.error(err);

                rej(error);

            }
            
            
        });
    }

    /**
     * formata data e hora (YYYY-MM-DD HH:mm:ss)
     */
    _DATETIME() {
        const m =  moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
        console.log(m);
        return m;

    }
}