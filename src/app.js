import express from 'express';

import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';

import loginRouter from './routes/login';
import clientsRouter from './routes/clients';
import moviesRouter from './routes/movies';
import rentalsRouter from './routes/rentals';

var app = express();

app.use(logger('dev'));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', loginRouter);
app.use('/clients', clientsRouter);
app.use('/movies', moviesRouter);

app.use('/rentals', rentalsRouter);

module.exports = app;
